package unit004_arraylistparameters;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  - 

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import static java.lang.System.*;

public class MadLib
{
	public ArrayList<String> verbs;
	public ArrayList<String> nouns;
	public ArrayList<String> adjectives;
	public ArrayList<String> templato;
	public MadLib()
	{
		verbs = new ArrayList<String>();
		nouns = new ArrayList<String>();
		adjectives = new ArrayList<String>();
                templato = new ArrayList<String>();
	}

	public MadLib(String template)
	{
		//load each ArrayList
		this();    	// calls the default constructor above
		loadNouns();
		loadVerbs();
		loadAdjectives();
		
		
		Scanner mlBuilder = new Scanner(template);  // loads template into scanner
                String answer = "";
                String random = "";
                while (mlBuilder.hasNext())
                {
                    //for (int i = 0; i < template.length(); i ++)
                    //{
                        String s = mlBuilder.next();
                        
                        
                        if (s.equals("#"))
                        {
                        random = getRandomNoun();
                        answer = answer + random + (" ");
                        //replace char with variable nouns    
                        }
                        else if (s.equals("#."))
                        {
                        random = getRandomNoun();
                        answer = answer + random + (".");
                        }
                        else if (s.equals("@"))
                        {
                        random = getRandomVerb();
                        answer = answer + random + (" ");
                        //replace char with variable nouns    
                        }
                        else if (s.equals("&"))
                        {
                        random = getRandomAdjective();
                        answer = answer + random + (" ");    
                        }
                        else if (s.equals(" "))
                        {
                           answer = answer + "";    
                        }    
                        else 
                        {
                           answer = answer + s + (" ");    
                        }    
                    //}
                   
                   
                } 
                System.out.println(answer + (""));
		// insert code here to...
		// loop through each element of the template
		// determine whether the element needs to be printed as is
		// or if a random item needs to be pulled from a file... and printed  	
			
		
		
		
	
	
		
		
		
	}

	public void loadNouns()
	{
		try{
		// load appropriate ArrayList from appropriate file
		String currentNoun = "";
                Scanner loadTheNouns = new Scanner (new File("nouns.dat"));
                while (loadTheNouns.hasNext())
                {    
                    currentNoun = loadTheNouns.nextLine();
                    nouns.add(currentNoun);
                }
                
                
		
		}
		catch(Exception e)
		{
                    System.out.println("error: noun");
		}	

	}
	
	public void loadVerbs()
	{
		try{
		// load appropriate ArrayList from appropriate file	
                String currentVerb = "";
                Scanner loadTheVerbs = new Scanner (new File("verbs.dat"));
                while (loadTheVerbs.hasNext())
                {    
                    currentVerb = loadTheVerbs.nextLine();
                    verbs.add(currentVerb);
                }
                
                
		
                
	
	
	
		}
		catch(Exception e)
		{
                    System.out.println("error: verb");
		}
	}

	public void loadAdjectives()
	{
		try{
		// load appropriate ArrayList from appropriate file	
                    
                String currentAdjective = "";
                Scanner loadTheAdjectives = new Scanner (new File("adjectives.dat"));
                while (loadTheAdjectives.hasNext())
                {    
                    currentAdjective = loadTheAdjectives.nextLine();
                    adjectives.add(currentAdjective);
                }
                
                
		
		    
	
	
	
		}
		catch(Exception e)
		{
                    System.out.println("error: adjective");    
		}
	}

	public String getRandomVerb()
	{
            int spot = (int)(Math.random()*nouns.size());
            String randomVerb = nouns.get(spot);
            return randomVerb;
	}
	
	public String getRandomNoun()
	{
            int spot = (int)(Math.random()*nouns.size());
            String randomNoun = nouns.get(spot);
            return randomNoun;
            
	}
	
	public String getRandomAdjective()
	{
	    int spot = (int)(Math.random()*nouns.size());
            String randomAdjective = nouns.get(spot);
            return randomAdjective;
	}		

	public String toString()
	{
	   return "\n\n\n";
	}
}